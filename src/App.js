import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Weather from "./Components/Weather";
import WeatherDetail from "./Components/WeatherDetail";

export default function App() {
  return (

    <Router>
      <div>
        <Switch>
          <Route path="/" exact>
            <Weather />
          </Route>
          <Route path="/WeatherDetail">
            <WeatherDetail />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}
