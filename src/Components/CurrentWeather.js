import axios from "axios";
import { useState, useEffect } from "react";
import { useQuery } from "react-query";
import ForcastWeather from "./ForcastWeather";
import { BsArrowDown, BsArrowUp } from "react-icons/bs";
import { WiHumidity } from "react-icons/wi";
import {TbTemperatureCelsius} from 'react-icons/tb'
import {FaWind} from 'react-icons/fa'
import {GrStatusCriticalSmall} from 'react-icons/gr'

function CurrentWeather() {
  const API_KEY = process.env.REACT_APP_API_KEY;
  const [location, setLocation] = useState("");
  const [icon, setIcon] = useState("");
  const [forcatData, setForcatData] = useState();
  const url = `https://api.openweathermap.org/data/2.5/weather?q=${location}&appid=${API_KEY}&units=imperial`;
  const imgurl = `https://openweathermap.org/img/wn/${icon}@2x.png`;

  const ForcastUrl = `https://api.openweathermap.org/data/2.5/forecast?q=${location}&appid=${API_KEY}`;

  async function fetchPosts() {
    const { data } = await axios.get(url);
    console.log(data);
    console.log("esme shahr", data.name);
    localStorage.setItem("City name", data.name);
    console.log(status);
    setIcon(data.weather[0].icon);
    return data;
  }

  async function fetchForcast() {
    const { data } = await axios.get(ForcastUrl);
    setForcatData(data.list);
  }

  const {
    status: statusForcast,
    data: dataForcast,
    error: errorForcast,
    refetch: refetchForcast,
  } = useQuery("dataForcast", fetchForcast, {
    refetchOnWindowFocus: false,
    enabled: false,
  });

  const { status, data, isError, isLoading, refetch } = useQuery(
    "data",
    fetchPosts,
    {
      refetchOnWindowFocus: false,
      enabled: false,
      staleTime: 30 * 1000,
      refetchInterval: 30 * 1000,
    }
  );

  const searchLocation = (event) => {
    if (event.key === "Enter") {
      refetch();
      refetchForcast();
      setLocation("");
    }
  };
  return (
    <div className="App">
      <div className="search">
        <input
          value={location}
          onChange={(event) => setLocation(event.target.value)}
          onKeyPress={searchLocation}
          placeholder="Enter Location"
          type="text"
        />
      </div>
      {isLoading && <div>Loading...</div>}
      {isError && <div>Error!</div>}
      {status === "success" && (
        <div className="total-box">
          <div className="box-weather-now">
            <div className="first-line">
              {data.main && (
                <div className="Icon">
                  <img src={imgurl} width="90"></img>
                </div>
              )}
              <div className="Location">
                <p>{data.name}</p>
                {/* {data.weather ? <p>{data.weather[0].main}</p> : null} */}
              </div>
            </div>
            <div className="container">
              <div className="top">
                <div className="temp ">
                  {data.main ? (
                    <div className="box-current-temp">
                      <p className="current-temp">
                        {(((data.main.temp.toFixed() - 32) * 5) / 9).toFixed()}
                      </p>
                      <span className="temp-sign">°C</span>
                    </div>
                  ) : null}
                </div>
                <div className="low-high">
                  <div className="temp up-down">
                    {data.main ? (
                      <p>
                        <BsArrowUp />
                        {(
                          ((data.main.temp_max.toFixed() - 32) * 5) /
                          9
                        ).toFixed()}
                        °C
                      </p>
                    ) : null}
                  </div>
                  <div className="temp up-down">
                    {data.main ? (
                      <p>
                        <BsArrowDown />
                        {(
                          ((data.main.temp_min.toFixed() - 32) * 5) /
                          9
                        ).toFixed()}
                        °C
                      </p>
                    ) : null}
                  </div>
                </div>
              </div>

              {data.name != undefined && (
                <div className="bottom container">
                  <div className="row">
                    <div className="col">
                      <div className="feels humidity">
                        <TbTemperatureCelsius />
                        {data.main ? (
                          <p>
                            {(
                              ((data.main.feels_like.toFixed() - 32) * 5) /
                              9
                            ).toFixed()}
                            °C
                          </p>
                        ) : null}
                      </div>
                    </div>
                    <div className="col">
                      <div className="humidity up-down">
                        <WiHumidity />
                        {data.main ? (
                          <p>{data.main.humidity.toFixed()}%</p>
                        ) : null}
                      </div>
                    </div>
                  </div>

                  <div className="row second-row">
                    <div className="col">
                      <div className="wind humidity">
                        <FaWind />
                        {data.wind ? (
                          <p>{data.wind.speed.toFixed()} MPH</p>
                        ) : null}
                      </div>
                    </div>
                    <div className="col">
                      <p className="description humidity">
                        <GrStatusCriticalSmall />
                        {data.weather ? <p>{data.weather[0].main}</p> : null}
                      </p>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      )}

      <ForcastWeather forcastData={forcatData} />
    </div>
  );
}

export default CurrentWeather;
