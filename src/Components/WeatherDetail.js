import React, { useEffect } from "react";

const WeatherDetail = () => {
  let selectItem = localStorage.getItem("item");
  let obj = JSON.parse(selectItem);
  let selectedCity = localStorage.getItem("City name");
  return (
    <div>
      <p>{selectedCity}</p>
      <h1>{(((Number(obj.main.temp.toFixed()) - 32) * 5) / 9).toFixed()}°C</h1>
      <img
        src={`https://openweathermap.org/img/wn/${obj.weather[0].icon}@2x.png`}
        width="120"
        height="100"
      ></img>
      <p>wind speed: {obj.wind.speed}</p>
      <p>humidity: {obj.main.humidity}</p>
      <p>visibility: {obj.visibility}</p>
      <p>wind gust: {obj.wind.gust}</p>
    </div>
  );
};

export default WeatherDetail;
