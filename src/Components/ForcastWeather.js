import React, { useEffect } from "react";
import timestamp from "unix-timestamp";
import { useHistory } from "react-router-dom";

const ForcastWeather = ({ forcastData }) => {
  const history = useHistory();
  useEffect(() => {
    const timestamp = require("unix-timestamp");
    console.log("timestamp UseEffect:>> ", timestamp.day);

    var test = timestamp.toDate(1668859200).getUTCDay();
    console.log("test", test);
  }, []);
  var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

  useEffect(() => {
    var timestamp = 1668859200;
    var a = new Date(timestamp * 1000);

    var dayOfWeek = days[a.getDay()];
    console.log("dayOfWeek :>> ", dayOfWeek);
  }, [forcastData]);

  if (forcastData) {
    var slicedArray = forcastData.slice(0, 6);
    console.log("slicedArray :>> ", slicedArray);
  }

  const selectWeather = (item) => {
    history.push(`/weatherDetail/:${item.dt}`);
    localStorage.setItem("item", JSON.stringify(item));
  };

  return (
    <div className="total-daily">
      {slicedArray &&
        slicedArray.map((item) => (
          <div className="daily-weather" onClick={() => selectWeather(item)}>
            <p>{days[timestamp.toDate(item.dt).getUTCDay()]}</p>
            <p>{timestamp.toDate(item.dt).getHours()}</p>
            <img
              src={`https://openweathermap.org/img/wn/${item.weather[0].icon}@2x.png`}
              width="120"
              height="100"
            ></img>
          </div>
        ))}
    </div>
  );
};

export default ForcastWeather;
